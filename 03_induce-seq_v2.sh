#!/usr/bin/env bash
clear

# Setting variables
method=INDUCE_SEQ
platform=NEXTSEQ550
BAM=BAM
SAM=SAM
BED=BED
FASTQ=FASTQ
SUM=summary
tmpdir=tmpdir
blacklist=accessory_files/hg19.blacklist.bed
chromsizes=accessory_files/hg19.chrom.sizes.bed
chromends=accessory_files/hg19.chrom.ends.bed
refseq=/home/ec2-user/bwa_refseq/Homo_sapiens_assembly19.fasta
quality=30
threads=16

#Detect FASTQ files
for file in "$FASTQ"/*_trimmed.fq.gz; do (
filename=$(echo "$file" | awk -F'[/]' '{print $2}')
echo "$filename" >> filelist.txt
experiment=$(echo "$filename" | awk -F'[.]' '{print $1}')
echo "Found" "$filename" "from experiment:" "$experiment"
# rm filelist_"$experiment" #comment this line if you want to test whether the above functions actually finds your files

# Generate folders
mkdir -p "$experiment"
mkdir -p "$experiment"/preprocessing/"$BAM"
BAM="$experiment"/preprocessing/"$BAM"
mkdir -p "$experiment"/preprocessing/"$SAM"
SAM="$experiment"/preprocessing/"$SAM"
mkdir -p "$experiment"/preprocessing/"$BED"
BED="$experiment"/preprocessing/"$BED"
mkdir -p "$experiment"/preprocessing/"$SUM"
SUM="$experiment"/preprocessing/"$SUM"
mkdir -p "$experiment"/preprocessing/"$tmpdir"
tmpdir="$experiment"/preprocessing/"$tmpdir"
mkdir -p summary

# ALIGNMENT
if [[ ! -e "$SAM"/"$experiment".sam && ! -e "$tmpdir"/"$experiment".sam ]]; then
  bwa mem -t "$threads" -M -R '@RG\tID:"$method"\tPL:"$platform"\tPU:0\tLB:"$method"\tSM:"$experiment"' "$refseq" "$FASTQ"/"$filename" > "$tmpdir"/"$experiment".sam
  awk '$6 !~ /[0-9]S/{print}' "$tmpdir"/"$experiment".sam | samtools view -Shu -q "$quality" -F 256 -@ "$threads" - |
  samtools sort -@ "$threads" -m 4G - -o "$BAM"/"$experiment".q30.srt.bam
  samtools index "$BAM"/"$experiment".q30.srt.bam
  mv "$tmpdir"/"$experiment".sam "$SAM"/"$experiment".sam

###############################################################
# Convert BAM-to-BED
# Filter out blacklist regions, chromosome ends & non-canonical contigs
bedtools bamtobed -i "$BAM"/"$experiment".q30.srt.bam |
bedtools intersect -v -bed -a - -b "$blacklist" |
bedtools intersect -v -bed -a - -b "$chromends" |
bedtools intersect -wa -a - -b "$chromsizes" > "$BED"/"$experiment".q30.srt.bed

###############################################################
# Split by strand & remove optical duplicates
echo "Defining break ends and counting break events and locations"
awk '{ if ($6 == "+") 
print $1,$2,$3=$2+1,$4,$5,$6;
else
print $1,$2=$3-1,$3,$4,$5,$6 }' OFS="\t" "$BED"/"$experiment".q30.srt.bed |
LC_ALL=C sort --parallel="$threads" -k1,1 -k2,2n | #refer to above note on sorting large bed files on smaller instances
sed 's/:/ /g' | awk '{print $1"_"$2"_"$3"_"$4":"$5":"$6":"$7":"$8":"$9":"$10"_"$11"_"$12, $8, $10 }' | #substitute everything that matches the regular expression ~ with a space, globally
awk 'NR==1{p=$2;q=$3;next}    #p=tile and q=y coordinate
{print $1, $2-p, $3-q; p=$2 ; q=$3}' |
sed 's/_/ /g' | 
awk -v BED="$BED" '{ if (($7 == 0 || $7 == 1) && $8 <= 40 && $8 >= -40) 
{print $1, $2, $3, $4, $5, $6 > BED"/optical_duplicates.txt"} 
else {print $1, $2, $3, $4, $5, $6}}' OFS="\t" > "$BED"/"$experiment".breakends.bed
# Flatten breakends files using bedtools merge and count breaks at each unique location
bedtools merge -s -c 2,6 -o count,distinct -i "$BED"/"$experiment".breakends.bed | awk -v var="$experiment" '{print $1,$2,$3,var"_"(FNR FS),$5,$4}' OFS="\t" > "$BED"/"$experiment".breakcount.bed

echo "Breakcounts files ready for secondary analysis"

#################################################################
# Generate a Summary for each dataset
echo "Writing summary file"
summaryfile="$SUM"/"$experiment"_summary
# Retreive alignment stats using samtools flagstat
echo "Alignment statistics:" > "$summaryfile"
samtools flagstat "$SAM"/"$experiment".sam >> "$summaryfile"
echo "" >> "$summaryfile"
# Retreive alignment statistics from BAM after filtering
echo "Alignment statistics after filtering:" >> "$summaryfile"
samtools flagstat "$BAM"/"$experiment".q30.srt.bam >> "$summaryfile"
echo "" >> "$summaryfile"
# Count total number of breaks
echo "Number of tagged break-ends:" >> "$summaryfile"
wc -l "$BED"/"$experiment".breakends.bed | cut -d' ' -f1 >> "$summaryfile"
echo "" >> "$summaryfile"
# Count total number of breaks per strand
echo "Number of tagged break-ends on the forward strand:" >> "$summaryfile"
awk '{ if ($6 == "+") print}' "$BED"/"$experiment".breakends.bed | wc -l >> "$summaryfile"
echo "" >> "$summaryfile"

echo "Number of tagged break-ends on the reverse strand:" >> "$summaryfile"
awk '{ if ($6 == "-") print}' "$BED"/"$experiment".breakends.bed | wc -l >> "$summaryfile"
echo "" >> "$summaryfile"
# Count total number of unique DSB locations
echo "Number of DSB locations:" >> "$summaryfile"
wc -l "$BED"/"$experiment".breakcount.bed | cut -d' ' -f1 >> "$summaryfile"
echo "" >> "$summaryfile"
# Calculate average break frequency
echo "Average break frequency at unique locations:" >> "$summaryfile"
awk '{x+=$5} END{print x/NR}' "$BED"/"$experiment".breakcount.bed >> "$summaryfile"
echo "" >> "$summaryfile"
# Generate a break frequency histogram to summarize break data
echo "Histogram breaks/location and number of locations:" >> "$summaryfile"
awk '{printf "%0.0f\n", $5 }' "$BED"/"$experiment".breakcount.bed | sort -k1,1n | uniq -c | awk '{print $2,$1}' >> "$summaryfile"
# Move summary file to top-level summary folder for easy access
cp "$summaryfile" summary/"$experiment".summary.txt

else
  echo "SAM file found in:" "$tmpdir"/"$experiment".sam "Alignment in progress, skipping."
fi
)
done

